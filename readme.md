# Infra project

## this project is responsible to create our AWS environment using IaC with Terraform

* to make it easier for us to test, I decided to do not use the AWS CLI to configure credentials/region but instead only export its environments as per item 3 on pre-requirements section.

## Pre-requirements

1. Install Terraform - on mac use:
    ```
    brew tap hashicorp/tap
    brew install hashicorp/tap/terraform
    ```
2. Create an ssh key to access our ec2
    ```
    ssh-keygen -f /Users/youruser/.ssh/tradedepot_rsa -t rsa
    ```
3. export env variables(note that those values are not supported to be here, only here for testing purpose)
    ```
    export AWS_ACCESS_KEY_ID=AKIAZRAIIDKOFAAW35BI
    export AWS_SECRET_ACCESS_KEY=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
    export AWS_DEFAULT_REGION=ap-southeast-2
    ```

## Running

* access the env folder via command line, in this case dev ``cd ...../infra/dev``
* run ``terraform init`` which validates your .tf files
* run ``terraform apply`` which will apply the .tf files into our account using our AWS env keu values.
