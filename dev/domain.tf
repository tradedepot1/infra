module "dns-zones" {
  source = "infrablocks/dns-zones/aws"

  domain_name = "thinksoft.nz"
  private_domain_name = "thinksoft.nz"

  # Default VPC
  private_zone_vpc_id = aws_security_group.security_group.vpc_id
  private_zone_vpc_region = "ap-southeast-2"
}
