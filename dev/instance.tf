variable "key_path" {
  default = "~/.ssh/tradedepot_rsa.pub"
}

resource "aws_key_pair" "tradedepot-pub-key" {
  key_name   = "tradedepot-pub-key"
  public_key = file(var.key_path)
}

resource "aws_instance" "instance" {
  ami           = "ami-0b21dcff37a8cd8a4" # Ubuntu 22.04 LTS (64-bit x86)  Free Tier eligible
  instance_type = "t2.micro" # Free Tier eligible

  subnet_id                   = "subnet-0e3921576ea9083c0"
  vpc_security_group_ids      = [aws_security_group.security_group.id]
  associate_public_ip_address = true

  key_name = aws_key_pair.tradedepot-pub-key.id

  root_block_device {
    volume_size           = 50
    delete_on_termination = true
  }

  tags = {
    Name = "DEV-Server"
  }

  user_data = <<-EOF
    #!/bin/bash
    sudo apt-get update -y
    curl -fsSL https://get.docker.com -o get-docker.sh
    sudo sh get-docker.sh
    sudo apt-get update
    sudo apt-get install docker-compose-plugin
    sudo apt install docker-compose -y
  EOF

  monitoring              = true
  disable_api_termination = false
}



